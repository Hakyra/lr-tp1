#include "Scene.h"

#include <fstream>

#include "Objet.h"
#include "Sphere.h"
#include "Plan.h"

Scene::Scene(std::string filename) {
    std::ifstream ifs(filename, std::ifstream::in);

    Materiau courant;

    while (!ifs.eof()) {
        std::string type;
        ifs >> type;

        if(type == "fond") {
            float r, v, b;
            ifs >> r >> v >> b;
            fond = Couleur(r, v, b);
        } else if (type == "materiau") {
            float aR, aV, aB, kd, ks, s;
            ifs >> aR >> aV >> aB >> kd >> ks >> s;
            courant = Materiau(Couleur(aR, aV, aB), kd, ks, s);
        } else if (type == "source") {
            float x, y, z, r, v, b;
            ifs >> x >> y >> z >> r >> v >> b;
            source = Source(Intensite(r, v, b), Point(x, y, z));
        } else if (type == "plan") {
            float a, b, c, d;
            ifs >> a >> b >> c >> d;
            objets.push_back(new Plan(courant, a, b, c, d));
        } else if (type == "sphere") {
            float x, y, z, r;
            ifs >> x >> y >> z >> r;
            objets.push_back(new Sphere(courant, Point(x, y, z), r));
        } else {
            std::string line;
            std::getline(ifs, line);
        }
    }
}

Scene::~Scene() {
    for (Objet* objet : objets)
        delete objet;
    /*
    for (unsigned int i = 0; i < objets.size(); i++)
        delete objet[i];
    */
    objets.clear();
}

void Scene::Print() {
    std::cout << "Contenu de la scène :" << std::endl;
    for (Objet* objet : objets) {
        objet->Print();
        std::cout << std::endl;
    }
    source.Print();
    std::cout << std::endl;
    std::cout << "La couleur de fond est ";
    fond.Print();
    std::cout << std::endl;
}

bool Scene::Intersecte(const Rayon& r, Intersection& inter) {
    bool found = false;

    for (Objet* o : objets) {
        Intersection temp;
        bool intersect = o->Intersect(r,temp);
        if (intersect && !found) {
            inter = temp;
            found = true;
        } else if (intersect && found) {
            if (temp.Dist < inter.Dist) {
                inter = temp;
            }
        }
    }

    return found;
}
